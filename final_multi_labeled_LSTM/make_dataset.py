import numpy as np
import io
import csv
import itertools
from collections import Counter
from sklearn.model_selection import train_test_split
from torchtext import data
import pickle
import re
import spacy
import en_core_web_sm
try:
    nlp = spacy.load('en', disable=['parser', 'ner'])
except:
    nlp = en_core_web_sm.load()

import torch
from model_config import model_argument

from preprocessing import preprocess_keywords

def split_train_test(text, label, keywords, test_size, seed):
    X_train, X_test, y_train, y_test = train_test_split(
            text, label, test_size=test_size, random_state=seed)

    # is exist keyword
    lbl_tr = np.sum(y_train, axis=0) > 0
    lbl_ts = np.sum(y_test, axis=0) > 0
    lbl_target = lbl_tr * lbl_ts

    print('{} target keywords'.format(sum(lbl_target)))

    y_train = y_train[:, lbl_target]
    y_test = y_test[:, lbl_target]
    keywords = keywords[lbl_target]

    return X_train, X_test, y_train, y_test, keywords

# text = X_test
# label = y_test
# keywords
def make_torchcsv_form(text, label, keywords, csvpath):
    with io.open(csvpath, 'w', encoding='utf8', newline='') as f:
        writer = csv.writer(f, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        keywords = keywords.astype('str').tolist()
        writer.writerow(['TEXT'] + keywords)

        for n, (t, l) in enumerate(zip(text, label)):
            t = [' '.join(x) for x in t]
            t = '. '.join(t) + '.'
            l = l.tolist()
            line = [t] + l
            writer.writerow(line)

def tokenizer(comment):
    MAX_CHARS = 20000
    comment = re.sub(
        r"[\*\"“”\n\\…\+\-\/\=\(\)‘•:\[\]\|’\!;]", " ", str(comment))
    comment = re.sub(r"[ ]+", " ", comment)
    comment = re.sub(r"\!+", "!", comment)
    comment = re.sub(r"\,+", ",", comment)
    comment = re.sub(r"\?+", "?", comment)
    if (len(comment) > MAX_CHARS):
        comment = comment[:MAX_CHARS]
    return [x.text for x in nlp.tokenizer(comment) if x.text != " "]


def get_dataset(keywords, file_train, file_test, fix_length=250, lower=False, vectors=None):
    comment = data.Field(
        sequential=True,
        fix_length=fix_length,
        tokenize=tokenizer,
        pad_first=True,
        tensor_type=torch.cuda.LongTensor,
        lower=lower
    )

    fields_csv = []
    fields_csv.append(('TEXT', comment))
    for l in keywords:        
        label_field = (l, data.Field(use_vocab=False,
                                     sequential=False,
                                     tensor_type=torch.cuda.FloatTensor)
        )
        # torch.cuda.ByteTensor

        fields_csv.append(label_field)

    train, test = data.TabularDataset.splits(
        path='datasets/', format='csv', skip_header=True,
        train=file_train, validation=file_test,
        fields=fields_csv)

    comment.build_vocab(
        train,
        max_size=50000,
        min_freq=20,
        vectors=vectors)

    with open('datasets/vocab.pickle', 'wb') as handle:
        pickle.dump(comment, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return train, test, comment

def get_iterator(dataset, batch_size, train=True, shuffle=True, repeat=False):
    
    dataset_iter = data.Iterator(
        dataset, batch_size=batch_size, device=0,
        train=train, shuffle=shuffle, repeat=repeat,
        sort=False
    )
    return dataset_iter


def get_iter(X_cluster, y_ins, batch_size=32):
    for i in range(0,len(X_cluster)-batch_size,batch_size):
        yield (X_cluster[i:i+batch_size], y_ins[i:i+batch_size])

def make_vocab(X_train):
    # 효율적으로 짤 수 있도록 변화 필요
    token_num = dict()
    for i, doc in enumerate(X_train):
        #doc = X_train[0]
        if i%10000 == 0:
            print('{}/{}'.format(i,len(X_train)))
        count = Counter(itertools.chain(*doc))
        for key in count.keys():
            if key not in token_num.keys():
                token_num[key] = count[key]
            else:
                token_num[key] += count[key]
    
    unique_token = [key for key in token_num.keys() if token_num[key] >= 10]       
    unique_token = sorted(unique_token)
    len(unique_token) # 20042
    
    vocab = dict()
    vocab['<PAD>'] = 0
    vocab['<UNK>'] = 1 
    for i, token in enumerate(unique_token):
        vocab[token] = i+2
   
    with open('datasets/vocab2.pickle', 'wb') as handle:
        pickle.dump(vocab, handle, protocol=pickle.HIGHEST_PROTOCOL)

    print('the number of unique tokens : {}'.format(len(vocab)))
    
    return vocab

def text_to_numerical(dataset, vocab):
    '''
    dataset = X_train
    vocab 
    Train = True
    '''
    length = 300
    nu_dataset = []
    for doc in dataset:
        nu_doc = []
        for sent in doc:
            for word in sent:
                if word in vocab.keys():
                    nu_doc.append(vocab[word])
                else:
                    nu_doc.append(vocab['<UNK>'])
        
        
        if len(nu_doc) >= length:
            nu_doc = nu_doc[:length]
        elif len(nu_doc) < length:
            pad = [0]*(length-len(nu_doc))
            for n_w in nu_doc:
                pad.append(n_w)
            nu_doc = pad
        
        if len(nu_doc) != length:
            print(len(nu_doc))
            print('Something Wrong')
            
        nu_dataset.append(np.array(nu_doc))
    nu_dataset = np.array(nu_dataset)
        
    return nu_dataset

def make_random_cluster(X, y, bootstrap, shuffle=True, count=True):
    
    k = model_argument['k']
    if shuffle:
        if bootstrap:
            idx = np.random.choice(range(len(X)), len(X), replace=True)
        else:
            idx = np.random.choice(range(len(X)), len(X), replace=False)

    else:
        idx = np.arange(len(X))
    
    X_cluster = []
    y_ins = []
    for i in range(0,len(X)-k,k):
         
        X_cluster.append(X[idx[i:i+k]])
        y_ins.append(np.sum(y[idx[i:i+k]],0))
    
    X_cluster = np.array(X_cluster)
    y_ins = np.array(X_cluster)

    print('X cluster : {}, y_ins : {}'.format(X_cluster.shape, y_ins.shape))
    
    return X_cluster, y_ins
    
if __name__ == '__main__':
    label = np.load('datasets/label.npy')
    text = np.load('datasets/text.npy')
    keywords = np.load('datasets/keyword.npy')
    
    pre_keywords = preprocess_keywords(keywords)
    with open('datasets/preprocessed_keywords.pickle', 'wb') as f:
        pickle.dump(pre_keywords,f) 

    X_train, X_test, y_train, y_test, keywords = split_train_test(text, label, keywords,
                                                                  test_size=1000,
                                                                  seed=12345)
    make_torchcsv_form(X_train, y_train, keywords,
                       csvpath='datasets/scopus_ai_train.csv')

    make_torchcsv_form(X_test, y_test, keywords,
                       csvpath='datasets/scopus_ai_test.csv')

    np.save('datasets/scopus_ai_keywords.npy', keywords)


