import numpy as np
import pandas as pd
import csv
import itertools
from collections import Counter
from matplotlib import pyplot as plt
from nltk.corpus import stopwords
stopwords_ = stopwords.words('english')

import operator
from functools import reduce

from preprocess.utils import remove_garbage_characters
from preprocess.utils import documents2words
from preprocess.utils import remove_stopwords, re_st
from preprocess.utils import lemmatization, lemm

import pickle

def read_tsv(path):
    data = []
    with open(path, encoding='utf8') as f:
        tsvreader = csv.reader(f, delimiter="\t", quoting=csv.QUOTE_NONE)
        for n, line in enumerate(tsvreader):
            index = line[0]
            categories = line[1].split(' ')
            text = line[2] + '. ' + line[3]  # join title & abstract
            keywords = line[5]

            if '1702' in categories:
                categories = ', '.join(categories)
                data.append([index, categories, text, keywords])

    print('{} AI papers in the data'.format(len(data)))
    data = np.array(data)

    return data


def make_multi_label(data, min_cnt=100):
    keywords = data[:, 3]  # keywords

    keywords_split = [x.split(' | ') for x in keywords]
    # each paper has 12.26 keywords on average

    # keyword counting
    keywords_count = list(itertools.chain(*keywords_split))
    keyword_count = Counter(keywords_count)

    keyword_text = []
    keyword_cnt = []
    for n, (k, i) in enumerate(keyword_count.items()):
        if i > min_cnt:
            keyword_text.append(k)
            keyword_cnt.append(i)

    num_target = len(keyword_text)
    print('{} target keywords in data'.format(num_target))

    return make_multi_label_target(keywords_split, keyword_text)


def make_multi_label_target(keywords_split, keyword_text):
    num_target = len(keyword_text)
    keyword_text = np.array(keyword_text)
    target_matrix = np.zeros(shape=(len(keywords_split), num_target))

    for i, y in enumerate(keywords_split):
        label_pos = reduce(operator.add, [np.where(keyword_text == x)[0].tolist() for x in y])
        # assgin label
        target_matrix[i, label_pos] = 1

    return target_matrix, keyword_text


def preprocess_text(text):
    print('start preprocessing...')
    text = [x.split('©')[0] for x in text]
    text = remove_garbage_characters(text)
    text = documents2words(text)
    text = remove_stopwords(text, stopwords_)
    text = lemmatization(text, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'])

    # remove empty list
    for idx, x in enumerate(text):
        x = np.array(x)
        x = x[np.array([len(i) for i in x]) > 0]
        x = x.tolist()
        text[idx] = x

    return text

def preprocess_keywords(keywords):
    with open('datasets/vocab.pickle', 'rb') as handle:
        vocab = pickle.load(handle) 
    vocab = vocab.__dict__.get('vocab').stoi
    # list(vocab.keys())[0] - <unk>
    # list(vocab.keys())[1] - <pad>
    
    pre_keywords = dict()
    for i, l in enumerate(keywords):
        pre_keywords[i] = [l]
        if '/' in l:
            pre_keywords[i] = l.split('/')
        if '(' in l:
            tmp = l.split('(')
            tmp[1] = str(tmp[1])[:-1]
            pre_keywords[i] = tmp

    pre_labels = []
    num_labels = []    
    for i in range(len(pre_keywords)):
        if i%100 == 0:
            print('{}/{} ing'.format(i+1,len(pre_keywords)))
        pre_ = []
        num_ = []
        for label in pre_keywords[i]:
            new_label = []
            for w in label.split(' '):
                # lower
                w = w.lower()
                # remove stopwords
                w = re_st(w, stopwords_)
                if w != '_stopwords_':
                    new_label.append(w)
            new_label = ' '.join(new_label)
            # lemmatization
            new_label = lemm(new_label, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'])
            new_label = ' '.join(new_label)
            pre_.append(str(new_label))
        
        for label in pre_:
            new_label2 = [int(vocab[w]) for w in label.split(' ')] 
            num_.append(new_label2)
        
        pre_labels.append(pre_)
        num_labels.append(num_)
    
    idx = range(len(pre_labels))
    result = {'index':idx,
                   'keywords':keywords,
                   'prprocessed':pre_labels,
                   'number':num_labels}
    
    result = pd.DataFrame(data=result)
    return result

if __name__ == '__main__':
    path = 'Z:/1. 프로젝트/2018_KISTI/scopus_data/여운동_20180810/여운동_201808.tsv'
    data = read_tsv(path)
    label, keywords = make_multi_label(data, min_cnt=100)
    text = preprocess_text(data[:, 2])

    np.save('datasets/label.npy', label)
    np.save('datasets/text.npy', text)
    np.save('datasets/keyword.npy', keywords)
    
