import numpy as np
import torch
from model_config import model_argument
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score

def y_to_dist(y):
    y = torch.sum(y, dim=0, keepdim=True)
    y = y/torch.sum(y)
    return y

def make_unk(x, y, pre_keywords):
    y = np.array(y).T
    #y.shape 1248,20
    
    for j in range(x.shape[1]):
        label_indice = np.where(y[:,j] == 1)[0]
        unk_list = [num for idx in label_indice for num in pre_keywords.loc[idx,'number']]
        #[[1369], [8, 4, 9698]]
        for unk_l in unk_list:
            # unk_l = [4, 9698]
            if unk_l[0] in x[:,j]:
                where = np.where(x[:,j] == unk_l[0])[0]
                for whe in where:
                    #whe = 207
                    if np.sum(np.array(x[whe:whe+len(unk_l):,j]) == np.array(unk_l)) == len(unk_l):
                        x[whe:whe+len(unk_l),j] = 0    
    return x

def measure(Y, P, k=3):
    top_k_accs = []
    for target, pred in zip(Y,P):
        top_k_pred_indice = np.argsort(pred)[-k:][::-1]
        label_indice = np.argwhere(target)[:,0]  
        
        top_k_acc = np.sum([True for idx in top_k_pred_indice if idx in label_indice])/k*100
        top_k_accs.append(top_k_acc)
    av_top_k_acc = np.mean(top_k_accs)
        
    recalls, precisions = [], []
    for target, pred in zip(Y, P):
        pred = pred > 0
        pred = pred.astype(int)
        
        target = target > 0
        target = target.astype(int)
        recall = recall_score(target, pred)
        precision = precision_score(target, pred)
        recalls.append(recall)
        precisions.append(precision)
    av_recall = np.mean(recalls)
    av_precision = np.mean(precisions)
    return av_top_k_acc, av_recall, av_precision

def learn(model, data, optimizer, criterion, pre_keywords):
    model.train()
    total_loss = 0

    for batch_num, batch in enumerate(data): # data=train_iter      
        print('{} batch start'.format(batch_num))
        model.zero_grad()

        y = []
        for k in batch.fields:
            if k == 'TEXT':
                x = batch.__dict__.get(k)
            else:
                y.append(batch.__dict__.get(k))
        
        y = torch.stack(y, dim=1)
        x = make_unk(x, y, pre_keywords)

        y = y_to_dist(y)
        
        #x.shape  (250,20) y.shape (1,1248)
        
        dist = model(x)
        dist = dist.view(-1, model_argument['nlabels'])
        loss = criterion(dist, y)
        total_loss += float(loss)
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), model_argument['clipgrad'])
        optimizer.step()

    return total_loss / len(data)


def evaluate(model, data, criterion, pre_keywords, type='Valid', save=False):
    model.eval()
    total_loss = 0

    Y = []
    P = []
    with torch.no_grad():
        for batch_num, batch in enumerate(data): #data = test_iter
            print('{} batch start'.format(batch_num))
            y = []
            for k in batch.fields:
                if k == 'TEXT':
                    x = batch.__dict__.get(k)
                else:
                    y.append(batch.__dict__.get(k))

            y = torch.stack(y, dim=1)
            x = make_unk(x, y, pre_keywords)
            y = y_to_dist(y)

            dist = model(x)
            dist = dist.view(-1, model_argument['nlabels'])
            loss = criterion(dist, y)
            total_loss += float(loss)

            # TODO: recall / precision
            prob = dist
            P.append(prob.cpu().data.numpy())
            Y.append(y.cpu().data.numpy())

    P = np.concatenate(P, axis=0)
    Y = np.concatenate(Y, axis=0)
    
    av_top_k_acc, av_recall, av_precision = measure(Y, P, k=3)

    print()
    print("[{} loss]: {:.5f}".format(type, total_loss / len(data)))
    print("[{} top k accuracy]: {:.5f}".format(type, av_top_k_acc))
    print("[{} recall]: {:.5f}".format(type, av_recall))
    print("[{} precision]: {:.5f}".format(type, av_precision))

    if save:
        np.save('training_history/mlc_20180903_test_target.npy', Y)
        np.save('training_history/mlc_20180903_test_predict.npy', P)

        k = []
        for x in data.dataset.fields:
            if x != 'TEXT':
                k.append(x)

        np.save('training_history/mlc_20180903_test_dict.npy', k)

    return total_loss / len(data), av_top_k_acc, av_recall, av_precision


def load_pretrained_vectors(dim):
    pretrained_GloVe_sizes = [50, 100, 200, 300]

    if dim in pretrained_GloVe_sizes:
        # Check torchtext.datasets.vocab line #383
        # for other pretrained vectors. 6B used here
        # for simplicity
        name = 'glove.{}.{}d'.format('6B', str(dim))
        return name
    return None
