# https://github.com/mttk/rnn-classifier/blob/master/model.py
import torch
import torch.nn as nn
import torch.nn.functional as F
import math
from model_config import model_argument

RNNS = ['LSTM', 'GRU']

class Encoder(nn.Module):
    def __init__(self, embedding_dim, hidden_dim, nlayers=1, dropout=0.,
                bidirectional=True, rnn_type='GRU'):
        super(Encoder, self).__init__()
        self.bidirectional = bidirectional
        assert rnn_type in RNNS, 'Use one of the following: {}'.format(str(RNNS))
        rnn_cell = getattr(nn, rnn_type) # fetch constructor from torch.nn, cleaner than if
        self.rnn = rnn_cell(embedding_dim, hidden_dim, nlayers, 
                            dropout=dropout, bidirectional=bidirectional)

    def forward(self, input, hidden=None):
        return self.rnn(input, hidden)

class Attention(nn.Module):
    def __init__(self, query_dim, key_dim, value_dim):
        super(Attention, self).__init__()
        self.scale = 1. / math.sqrt(query_dim)

    def forward(self, query, keys, values):
        # Query = [BxQ]
        # Keys = [TxBxK]
        # Values = [TxBxV]
        # Outputs = a:[TxB], lin_comb:[BxV]

        # Here we assume q_dim == k_dim (dot product attention)

        query = query.unsqueeze(1) # [BxQ] -> [Bx1xQ]
        keys = keys.transpose(0,1).transpose(1,2) # [TxBxK] -> [BxKxT]
        energy = torch.bmm(query, keys) # [Bx1xQ]x[BxKxT] -> [Bx1xT]
        energy = F.softmax(energy.mul_(self.scale), dim=2) # scale, normalize

        values = values.transpose(0,1) # [TxBxV] -> [BxTxV]
        linear_combination = torch.bmm(energy, values).squeeze(1) #[Bx1xT]x[BxTxV] -> [BxV]
        return energy, linear_combination   

class TimeDistributed(nn.Module):
    def __init__(self, embedding, encoder, attention, hidden_dim, num_classes):
        super(TimeDistributed, self).__init__()
        self.embedding = embedding
        self.encoder = encoder
        self.attention = attention
        self.decoder = nn.Linear(hidden_dim*model_argument['k'], num_classes) #hidden_dim = 100 num_classes=1248
        self.softmax = nn.Softmax()
    
        size = 0
        for p in self.parameters():
            size += p.nelement()
        print('Total param size: {}'.format(size))
        
    
    def forward(self, input):
        # input = x # 250,20
        
        TD_result = []
        for i in range(input.size()[1]):
            # i = 0
            
            TD_input = input[:,i]
            emb_input = self.embedding(TD_input) #250, 50
            outputs, hidden = self.encoder(emb_input.unsqueeze(1)) # outputs.size() #250,1,100, hidden.size() #1,1,100
            if isinstance(hidden, tuple): # LSTM
                hidden = hidden[1] # take the cell state
            if self.encoder.bidirectional: # need to concat the last 2 hidden layers
                hidden = torch.cat([hidden[-1], hidden[-2]], dim=1)
            else:
                hidden = hidden[-1]

            energy, linear_combination = self.attention(hidden, outputs, outputs) #energy.shape 1, 1, 250, linear_combination.shape 1,100
            TD_result.append(linear_combination)
        
        #len(TD_result)
        TD_output = torch.cat(TD_result, dim=1)
        TD_output_flatten = TD_output.view(-1)
        logits = self.decoder(TD_output_flatten)
        dist = self.softmax(logits)

        return dist

    